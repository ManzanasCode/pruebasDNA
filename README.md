# Proyecto ADN Analisis

## Instalación

- Instalar nodeJS version superior a v.14
- Descargar repositorio
- Una vez descargado el repo colocarse en la carpeta del proyecto y ejecutar el comando: npm i 
. Despues ejecutar el comando: npm run start
. El proyecto corre por el puerto 4000 y este puerto se puede cambiar en el archivo "index.ts"
. La base de datos es SQLITE y se encuentra en el proyecto

## Descripción

- Este proyecto es una api rest para buscar mutaciones en una cadena de ADN